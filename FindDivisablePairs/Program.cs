﻿using System;

namespace FindDivisablePairs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter The Count of Integers: ");
            int Size = Int32.Parse(Console.ReadLine());
            int[] BunchOfIntegers = new int[Size];
            for (int i = 0; i < Size; i++)
            {
                Console.Write(i + ": ");
                BunchOfIntegers[i] = Int32.Parse(Console.ReadLine());
            }
            int Count = CountDivisiblePairs(BunchOfIntegers, 4);
            Console.WriteLine("Count of Pairs Divisible by 4 is: {0}", Count);
        }

        static int CountDivisiblePairs(int[] BunchOfIntegers, int Divisor)
        {
            int PairCount = 0;
            int[] RemainderArray = new int[Divisor];
            //Fix all Remainder counts
            for(int i = 0; i < BunchOfIntegers.Length; i++)
                RemainderArray[BunchOfIntegers[i] % Divisor]++;
            //Outcome is the Count of Pairs Having Sum of Remainders equal to the Divisor
            for (int i = 0; i < Divisor / 2; i++)
                if (i == 0)
                    PairCount += RemainderArray[i] * RemainderArray[i] - 1;
                //Prevent i from being equal to Divisor - i to count these pairs once
                else if (i > 0 && i != Divisor - i)
                    PairCount += RemainderArray[i] * RemainderArray[Divisor - i];
            //If the Divisor is even, numbers having divisor / 2 remainder will be paired with themselves as much as possible
            if (Divisor % 2 == 0)
                PairCount += RemainderArray[Divisor / 2] * RemainderArray[Divisor / 2] - 1;
            return PairCount;
        }
    }
}